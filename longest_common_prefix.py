def longest_common_prefix(list):
    """
    Write a function to find the longest common prefix string amongst an list of strings. 
    If there is no common prefix, return an empty string "".
    Suppose a prefix is any combination of starting characters common amongst all strings in the list.

    doctest
    >>> longuest_common_prefix(["hello", "helsinski", "henderson", "here", "helping"])
    "he"
    >>> longuest_common_prefix(["dog", "racecar", "bird", "fair", "cooling"])
    ""
    """
    list.sort()
    prefix = list[0]
    for string in list:

    return prefix


if __name__ == "__main_":
    import doctest
    doctest.testmod()
