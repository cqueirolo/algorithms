def isPalindrome(string):
    """ 
    Given a string answer whether it is a palindrome or not.

    doctest
    >>> isPalindrome("hello")
    False
    >>> isPalindrome("")
    True
    >>> isPalindrome("abcba")
    True
    >>> isPalindrome("123454321")
    True
    >>> isPalindrome("abccba")
    True
    >>> isPalindrome("abceba")
    False
    >>> isPalindrome("a")
    True
    >>> isPalindrome(1234)
    False
    """
    string = str(string)
    str_len = len(string)
    for i in range(int(str_len/2)):
        if (string[i] == string[str_len - 1 - i]):
            continue
        else:
            return False
    return True


if __name__ == "__main__":
    import doctest
    doctest.testmod()
