def number_of_words(string):
    """
    Given a string, return the number of words found. Word separators are spaces.

    doctest
    >>> number_of_words("")
    0
    >>> number_of_words(" ")
    0
    >>> number_of_words("a")
    1
    >>> number_of_words("a b")
    2
    >>> number_of_words(" a   b   ")
    2
    >>> number_of_words("aaaaaaaaaa 4")
    2
    >>> number_of_words("Hello how are you today?")
    5
    """
    string = str(string)
    words = 0
    last_visited = " "
    for index, char in enumerate(string):
        if char == " " and last_visited != " ":
            words += 1
        if index == len(string) - 1 and char != " ":
            words += 1
        last_visited = char
    return words


if __name__ == "__main__":
    import doctest
    doctest.testmod()
